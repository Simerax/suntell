module gitlab.com/Simerax/suntell

go 1.15

require (
	github.com/kelvins/sunrisesunset v0.0.0-20210220141756-39fa1bd816d5 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)
