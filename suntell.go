package main

import (
	"fmt"
	"os"
	"time"

	"github.com/kelvins/sunrisesunset"
	"github.com/spf13/pflag"
)

func Exit(args ...interface{}) {
	fmt.Println(args...)
	os.Exit(1)
}

func main() {

	latitude := pflag.Float64("latitude", 52.49, "Latitude")
	longitude := pflag.Float64("longitude", 13.44, "Longitude")
	offset := pflag.Int("offset", 0, "Computation is done via UTC time. Use the offset in case you do not live in the UTC timezone")
	isDay := pflag.Bool("isDay", false, "output 'true' or 'false'")

	if longitude == nil {
		Exit("No Longitude set!")
	}
	if latitude == nil {
		Exit("No Latitude set!")
	}

	pflag.Usage = func() {
		pflag.PrintDefaults()
	}

	pflag.Parse()

	p := sunrisesunset.Parameters{
		Latitude:  *latitude,
		Longitude: *longitude,
		UtcOffset: float64(*offset),
		Date:      time.Now().UTC(),
	}

	sunrise, sunset, err := p.GetSunriseSunset()

	if err != nil {
		Exit("Error while computing Sunset and Sunrise:", err.Error())
	}

	if *isDay {
		now := time.Now().UTC()
		now = now.Add(time.Hour * time.Duration(*offset))

		if now.Unix() >= sunrise.Unix() && now.Unix() <= sunset.Unix() {
			fmt.Print("true")
		} else {
			fmt.Print("false")
		}
		os.Exit(0)
	}
	fmt.Println("Sunrise:", sunrise.Format("15:04:05"))
	fmt.Println("Sunset:", sunset.Format("15:04:05"))
}
