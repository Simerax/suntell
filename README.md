# Suntell

Tells you the time of sunrise and sunset. 
Can also tell you if its day or night.

All the actual calculation is done by [Kelvins sunrisesunset](https://github.com/kelvins/sunrisesunset).
This is just a small CLI around it.


Get Sunrise and Sunset of Berlin (UTC+1)
```
    $ suntell --latitude 52.49 --longitude 13.44 --offset 1
    > Sunrise: 06:06:16
    > Sunset: 18:21:25
```

Is it currently day or night in Berlin?
```
    $ suntell --latitude 52.49 --longitude 13.44 --isDay
    > false
```
